#include "mask.h"
#include <stdio.h>


#define SET 1
#define UNSET 0

static int mask_owner, mask_group, mask_other;

int main() {


	int owner = 0;
	int group = 0;
	int other = 0;

	char answer;


	printf("Permiso de lectura para el owner? [s/n]");
	answer = getAnswer();
	if (answer == 's')
		setPermiso(&owner, 'r', SET);


	printf("Permiso de escritura para el owner? [s/n]");
	answer = getAnswer();
	if (answer == 's')
		setPermiso(&owner, 'w', SET);

	printf("Permiso de ejecucion para el owner? [s/n]");
	answer = getAnswer();
	if (answer == 's')
		setPermiso(&owner, 'x', SET);

	printf("Permiso de lectura para el group? [s/n]");
	answer = getAnswer();
	if (answer == 's')
		setPermiso(&group, 'r', SET);

	printf("Permiso de escritura para el group? [s/n]");
	answer = getAnswer();
	if (answer == 's')
		setPermiso(&group, 'w', SET);

	printf("Permiso de ejecucion para el group? [s/n]");
	answer = getAnswer();
	if (answer == 's')
		setPermiso(&group, 'x', SET);

	printf("Permiso de lectura para el other? [s/n]");
	answer = getAnswer();
	if (answer == 's')
		setPermiso(&other, 'r', SET);

	printf("Permiso de escritura para el other? [s/n]");
	answer = getAnswer();
	if (answer == 's')
		setPermiso(&other, 'w', SET);

	printf("Permiso de ejecucion para el other? [s/n]");
	answer = getAnswer();
	if (answer == 's')
		setPermiso(&other, 'x', SET);




	printf("\n\nPermisos:\t %d%d%d  \n\n", owner, group, other );

}
